package com.tinkoff.tinkoffnews.presentation.newsList.list

import android.support.v7.widget.RecyclerView
import android.view.View
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.util.html.HtmlUtil
import kotlinx.android.synthetic.main.item_news_list.view.*
import java.lang.ref.WeakReference

/**
 *   @author Azat Galiullin
 **/
class NewsListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(news: News, clickListenerWR: WeakReference<NewsListClickListener>) {
        with(itemView) {
            tv_title.text = HtmlUtil.getFormattedTextFromHtml(news.text)
            tv_date.text = news.defaultFormattedPublicationDate
            setOnClickListener { clickListenerWR.get()?.onNewsClick(news) }
        }
    }

}