package com.tinkoff.tinkoffnews.presentation.fullNews

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.method.ScrollingMovementMethod
import android.view.View
import com.tinkoff.tinkoffnews.R
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.presentation.base.BaseActivity
import com.tinkoff.tinkoffnews.util.html.HtmlUtil
import kotlinx.android.synthetic.main.activity_full_news.*
import kotlinx.android.synthetic.main.merge_progress.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.concurrent.TimeUnit

/**
 *   @author Azat Galiullin
 **/
class FullNewsActivity : BaseActivity() {

    private lateinit var viewModel: FullNewsViewModel

    companion object {

        private const val NEWS = "news"

        fun open(activity: Activity, news: News) {
            val intent = Intent(activity, FullNewsActivity::class.java)
            intent.putExtra(NEWS, news)
            activity.startActivity(intent)
        }
    }

    private var networkErrorDialog: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val news = loadArgs()
        createViewModel(news)
        title = news.text
        setContentView(R.layout.activity_full_news)
        initViews()
    }

    override fun bindViewModel() {
        super.bindViewModel()
        viewModel.loadingLiveData.observe(this, Observer {
            progress.visibility = if (it == true) View.VISIBLE else View.GONE
        })
        viewModel.loadedNewsLiveData.observe(this, Observer {
            tv_full_news_text.text = HtmlUtil.getFormattedTextFromHtml(it?.content ?: "")
        })
        viewModel.networkErrorLiveData.observe(this, Observer {
            if (it == true)
                showNetworkErrorDialog()
            else
                hideNetworkDialog()
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        networkErrorDialog = null
    }

    private fun showNetworkErrorDialog() {
        networkErrorDialog = Snackbar.make(tv_full_news_text, R.string.network_error, TimeUnit.MINUTES.toMillis(1).toInt())
                .setAction(R.string.retry) { viewModel.loadFullNews() }
                .also { it.show() }
    }

    private fun hideNetworkDialog() {
        networkErrorDialog?.dismiss()
    }

    private fun initViews() {
        tv_full_news_text.movementMethod = ScrollingMovementMethod()
    }

    private fun loadArgs(): News = intent.getParcelableExtra<News>(NEWS)

    private fun createViewModel(news: News) {
        viewModel = viewModel<FullNewsViewModel>(parameters = { parametersOf(news.id) }).value
    }
}