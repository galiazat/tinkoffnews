package com.tinkoff.tinkoffnews.presentation.newsList

import android.arch.lifecycle.MutableLiveData
import com.tinkoff.tinkoffnews.domain.news.NewsInteractor
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.presentation.base.BaseViewModel
import com.tinkoff.tinkoffnews.util.liveData.SingleLiveEvent
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulers

/**
 *   @author Azat Galiullin
 **/
class NewsListViewModel(rxSchedulers: RxSchedulers,
                        private val newsInteractor: NewsInteractor) : BaseViewModel(rxSchedulers) {

    val newsListLiveData = MutableLiveData<List<News>>()
    val newsListLoadingLiveData = MutableLiveData<Boolean>()
    val openFullNewsScreenEvent = SingleLiveEvent<News>()

    init {
        loadNewsList()
    }

    fun loadNewsList() {
        newsListLoadingLiveData.value = true
        newsInteractor.getNewsList()
                .map { newsList -> newsList.sortedByDescending { it.publicationDate } }
                .subscribeOn(rxSchedulers.networkScheduler)
                .subscribe { list ->
                    newsListLiveData.postValue(list)
                    newsListLoadingLiveData.postValue(false)
                }
                .also { disposables.add(it) }
    }

    fun onNewsClick(news: News) {
        openFullNewsScreenEvent.value = news
    }

}