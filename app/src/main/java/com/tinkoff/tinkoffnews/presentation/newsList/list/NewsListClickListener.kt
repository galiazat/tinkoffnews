package com.tinkoff.tinkoffnews.presentation.newsList.list

import com.tinkoff.tinkoffnews.domain.news.entity.News

/**
 *   @author Azat Galiullin
 **/
interface NewsListClickListener {

    fun onNewsClick(news: News)

}