package com.tinkoff.tinkoffnews.presentation.fullNews

import android.arch.lifecycle.MutableLiveData
import com.tinkoff.tinkoffnews.domain.news.NewsInteractor
import com.tinkoff.tinkoffnews.domain.news.entity.FullNews
import com.tinkoff.tinkoffnews.presentation.base.BaseViewModel
import com.tinkoff.tinkoffnews.util.liveData.SingleLiveEvent
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulers

/**
 *   @author Azat Galiullin
 **/
class FullNewsViewModel(private val newsId: Long,
                        private val interactor: NewsInteractor,
                        rxSchedulers: RxSchedulers) : BaseViewModel(rxSchedulers) {

    val loadingLiveData = MutableLiveData<Boolean>()
    val loadedNewsLiveData = MutableLiveData<FullNews>()
    //after recreate continue show network error
    val networkErrorLiveData = MutableLiveData<Boolean>()

    init {
        loadFullNews()
    }

    fun loadFullNews() {
        networkErrorLiveData.postValue(false)
        loadingLiveData.value = true
        interactor.getFullNews(newsId)
                .subscribeOn(rxSchedulers.networkScheduler)
                .subscribe({
                    loadedNewsLiveData.postValue(it)
                    loadingLiveData.postValue(false)
                }, {
                    networkErrorLiveData.postValue(true)
                    it.printStackTrace()
                    loadingLiveData.postValue(false)
                })
                .also { addDisposable(it) }
    }

}