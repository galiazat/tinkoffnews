package com.tinkoff.tinkoffnews.presentation.base

import android.arch.lifecycle.ViewModel
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 *   @author Azat Galiullin
 **/
abstract class BaseViewModel(protected val rxSchedulers: RxSchedulers) : ViewModel() {

    protected val disposables = CompositeDisposable()

    fun addDisposable(it: Disposable) {
        disposables.add(it)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

}