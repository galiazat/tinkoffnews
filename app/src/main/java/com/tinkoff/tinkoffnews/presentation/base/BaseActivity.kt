package com.tinkoff.tinkoffnews.presentation.base

import android.support.v7.app.AppCompatActivity

/**
 *   @author Azat Galiullin
 **/
abstract class BaseActivity : AppCompatActivity() {

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        bindViewModel()
    }

    protected open fun bindViewModel() {}

}