package com.tinkoff.tinkoffnews.presentation.newsList

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.tinkoff.tinkoffnews.R
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.presentation.base.BaseActivity
import com.tinkoff.tinkoffnews.presentation.fullNews.FullNewsActivity
import com.tinkoff.tinkoffnews.presentation.newsList.list.NewsListAdapter
import com.tinkoff.tinkoffnews.presentation.newsList.list.NewsListClickListener
import com.tinkoff.tinkoffnews.util.recyclerView.RecyclerViewBottomMargin
import kotlinx.android.synthetic.main.activity_news_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class NewsListActivity : BaseActivity(), NewsListClickListener {

    private var newsListAdapter: NewsListAdapter? = null
    private val viewModel by viewModel<NewsListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)
        initList()
    }

    override fun onNewsClick(news: News) {
        viewModel.onNewsClick(news)
    }

    private fun initList() {
        newsListAdapter = NewsListAdapter(this)
        rv_list.layoutManager = LinearLayoutManager(this)
        rv_list.adapter = newsListAdapter
        val marginBetweenItems = resources.getDimensionPixelSize(R.dimen.news_list_item_margin)
        rv_list.addItemDecoration(RecyclerViewBottomMargin(marginBetweenItems, 1))
        srl_refresh_layout.setOnRefreshListener { viewModel.loadNewsList() }
    }

    override fun bindViewModel() {
        super.bindViewModel()
        viewModel.newsListLoadingLiveData.observe(this, Observer { srl_refresh_layout.isRefreshing = it ?: false })
        viewModel.newsListLiveData.observe(this, Observer { list -> list?.let { updateNewsList(it) } })
        viewModel.openFullNewsScreenEvent.observe(this, Observer { news -> news?.let { FullNewsActivity.open(this, it) } })
    }

    private fun updateNewsList(list: List<News>) {
        newsListAdapter?.newsList = list
    }
}
