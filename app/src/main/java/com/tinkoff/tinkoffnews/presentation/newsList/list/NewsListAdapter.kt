package com.tinkoff.tinkoffnews.presentation.newsList.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.tinkoff.tinkoffnews.R
import com.tinkoff.tinkoffnews.domain.news.entity.News
import java.lang.ref.WeakReference

/**
 *   @author Azat Galiullin
 **/
class NewsListAdapter(newsListClickListener: NewsListClickListener) : RecyclerView.Adapter<NewsListViewHolder>() {

    var newsList: List<News> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    private val clickListenerWR = WeakReference(newsListClickListener)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsListViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_news_list, parent, false)
        return NewsListViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsListViewHolder, position: Int) {
        holder.bind(newsList[position], clickListenerWR)
    }

    override fun getItemCount() = newsList.size

}