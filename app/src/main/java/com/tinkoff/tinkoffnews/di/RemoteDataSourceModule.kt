package com.tinkoff.tinkoffnews.di

import com.tinkoff.tinkoffnews.data.network.createOkHttpClient
import com.tinkoff.tinkoffnews.data.network.createWebService
import com.tinkoff.tinkoffnews.data.network.news.NewsApi
import com.tinkoff.tinkoffnews.repositories.news.NewsNetworkRepository
import com.tinkoff.tinkoffnews.repositories.news.impl.NewsNetworkRepositoryImpl
import org.koin.dsl.module.module

/**
 *   @author Azat Galiullin
 **/

private const val BASE_URL = "https://api.tinkoff.ru/v1/"

val remoteRepositoryModule = module {
    factory<NewsNetworkRepository> { NewsNetworkRepositoryImpl(get()) }
}

val remoteDatasourceModule = module {
    single { createOkHttpClient() }
    single { createWebService<NewsApi>(get(), BASE_URL) }
}