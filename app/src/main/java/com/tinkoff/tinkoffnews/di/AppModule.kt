package com.tinkoff.tinkoffnews.di

import com.tinkoff.tinkoffnews.domain.news.NewsInteractor
import com.tinkoff.tinkoffnews.presentation.fullNews.FullNewsViewModel
import com.tinkoff.tinkoffnews.presentation.newsList.NewsListViewModel
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulers
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulersImpl
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 *   @author Azat Galiullin
 **/

val appModule = module {
    viewModel { NewsListViewModel(get(), get()) }
    viewModel { FullNewsViewModel(it.values[0] as Long, get(), get()) }
}

val interactorModule = module {
    factory { NewsInteractor(get(), get(), get()) }
}

val rxSchedulersModule = module {
    single<RxSchedulers> { RxSchedulersImpl() }
}

val defaultAppModules = listOf(appModule, interactorModule, remoteRepositoryModule,
        localDatasourceModule, dbRepositoryModule, rxSchedulersModule, remoteDatasourceModule)