package com.tinkoff.tinkoffnews.di

import android.arch.persistence.room.Room
import com.tinkoff.tinkoffnews.App
import com.tinkoff.tinkoffnews.data.db.AppDatabase
import com.tinkoff.tinkoffnews.data.db.news.NewsDao
import com.tinkoff.tinkoffnews.repositories.news.NewsDbRepository
import com.tinkoff.tinkoffnews.repositories.news.impl.NewsDbRepositoryImpl
import org.koin.dsl.module.module

/**
 *   @author Azat Galiullin
 **/

val localDatasourceModule = module {
    single { Room.databaseBuilder(App.instance, AppDatabase::class.java, AppDatabase.DB_NAME).build() }
    single<NewsDao> {
        val db: AppDatabase = get()
        db.newsDao()
    }
}

val dbRepositoryModule = module {
    factory<NewsDbRepository> { NewsDbRepositoryImpl(get()) }
}