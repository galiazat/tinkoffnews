package com.tinkoff.tinkoffnews.util.rxSchedulers

import io.reactivex.Scheduler

/**
 *   @author Azat Galiullin
 **/
interface RxSchedulers {

    val dbScheduler: Scheduler
    val networkScheduler: Scheduler
    val mainThreadScheduler: Scheduler

}