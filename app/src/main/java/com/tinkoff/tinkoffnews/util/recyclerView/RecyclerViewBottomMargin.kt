package com.tinkoff.tinkoffnews.util.recyclerView

import android.graphics.Rect
import android.support.annotation.IntRange
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * @author Azat Galiullin
 */

/**
 * constructor
 * @param margin desirable margin size in px between the views in the recyclerView
 * @param columns number of columns of the RecyclerView
 */
class RecyclerViewBottomMargin(@param:IntRange(from = 0) private val margin: Int,
                               @param:IntRange(from = 0) private val columns: Int) : RecyclerView.ItemDecoration() {

    /**
     * Set different margins for the items inside the recyclerView: no top margin for the first row
     * and no left margin for the first column.
     */
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State?) {
        //set bottom margin to all
        outRect.bottom = margin
    }
}
