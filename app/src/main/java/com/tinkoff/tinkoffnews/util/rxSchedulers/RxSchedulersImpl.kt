package com.tinkoff.tinkoffnews.util.rxSchedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 *   @author Azat Galiullin
 **/
class RxSchedulersImpl : RxSchedulers {
    override val dbScheduler: Scheduler
        get() = Schedulers.io()
    override val networkScheduler: Scheduler
        get() = Schedulers.io()
    override val mainThreadScheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}