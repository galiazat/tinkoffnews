package com.tinkoff.tinkoffnews.util.date

import kotlin.reflect.KProperty

/**
 *   @author Azat Galiullin
 **/
class DefaultFormattedDateDelegate(private val date: Long) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return DateUtils.getDateInDefaultFormat(date)
    }
}