package com.tinkoff.tinkoffnews.util.html

import android.text.Html
import android.text.Spanned

/**
 *   @author Azat Galiullin
 **/
object HtmlUtil {

    fun getFormattedTextFromHtml(text: String): Spanned {
        return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
    }

}