package com.tinkoff.tinkoffnews.util.date

import java.text.DateFormat
import java.util.*

/**
 *   @author Azat Galiullin
 **/
object DateUtils {

    private val defaultDateFormat by lazy { DateFormat.getDateTimeInstance() }

    fun getDateInDefaultFormat(ms: Long) = defaultDateFormat.format(Date(ms))

}