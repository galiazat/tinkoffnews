package com.tinkoff.tinkoffnews.data.network.entity

/**
 *   @author Azat Galiullin
 **/
data class BaseResponse<T>(val resultCode: String,
                           val payload: T?,
                           val trackingId: String)

class EmptyPayloadException(resultCode: String): Exception("empty payload in response with status: $resultCode")