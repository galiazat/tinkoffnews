package com.tinkoff.tinkoffnews.data.db.news

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.tinkoff.tinkoffnews.domain.news.entity.News
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
@Dao
interface NewsDao {

    @Insert
    fun insertNews(list: List<News>)

    @Query("SELECT * from news")
    fun getNews(): Single<List<News>>

    @Query("DELETE FROM news")
    fun clearTable()


}