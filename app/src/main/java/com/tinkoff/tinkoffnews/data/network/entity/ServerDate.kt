package com.tinkoff.tinkoffnews.data.network.entity

import android.arch.persistence.room.TypeConverter
import com.google.gson.annotations.SerializedName
import com.tinkoff.tinkoffnews.util.date.DateUtils


/**
 *   @author Azat Galiullin
 **/
data class ServerDate(@SerializedName("milliseconds") val ms: Long) : Comparable<ServerDate> {
    override fun compareTo(other: ServerDate) = compareValues(ms, other.ms)
    val defaultFormattedDate: String
        get() = DateUtils.getDateInDefaultFormat(ms)
}

class PublicationDateConverter {
    @TypeConverter
    fun from(date: ServerDate): String {
        return date.ms.toString()
    }

    @TypeConverter
    fun to(data: String): ServerDate {
        return ServerDate(data.toLong())
    }

}