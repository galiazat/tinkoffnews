package com.tinkoff.tinkoffnews.data.network.news.entity

import com.tinkoff.tinkoffnews.data.network.entity.ServerDate
import com.tinkoff.tinkoffnews.domain.news.entity.FullNews

data class FullNewsRO(val title: NewsTitleRO,
                      val creationDate: ServerDate,
                      val lastModificationDate: ServerDate,
                      val content: String,
                      val bankInfoTypeId: Int,
                      val typeId: String) {

    fun convertToFullNews() = FullNews(title.converToNewsTitle(), creationDate.ms, lastModificationDate.ms,
            content, bankInfoTypeId, typeId)

}