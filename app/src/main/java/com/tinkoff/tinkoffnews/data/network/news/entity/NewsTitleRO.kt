package com.tinkoff.tinkoffnews.data.network.news.entity

import com.tinkoff.tinkoffnews.data.network.entity.ServerDate
import com.tinkoff.tinkoffnews.domain.news.entity.NewsTitle

data class NewsTitleRO(val id: String,
                       val name: String,
                       val text: String,
                       val publicationDate: ServerDate,
                       val bankInfoTypeId: Int) {

    fun converToNewsTitle() = NewsTitle(id, name, text, publicationDate.ms, bankInfoTypeId)

}
