package com.tinkoff.tinkoffnews.data.network.news.entity

import com.tinkoff.tinkoffnews.data.network.entity.ServerDate
import com.tinkoff.tinkoffnews.domain.news.entity.News

/**
 *   @author Azat Galiullin
 **/
data class NewsRO(val id: String,
                  val name: String,
                  val text: String,
                  val publicationDate: ServerDate,
                  val bankInfoTypeId: Int) {

    fun convertToNews() = News(id.toLong(), name, text, publicationDate.ms, bankInfoTypeId)
}