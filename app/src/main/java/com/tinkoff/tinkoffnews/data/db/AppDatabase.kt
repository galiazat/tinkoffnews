package com.tinkoff.tinkoffnews.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.tinkoff.tinkoffnews.data.db.news.NewsDao
import com.tinkoff.tinkoffnews.domain.news.entity.News

/**
 *   @author Azat Galiullin
 **/
@Database(entities = [(News::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsDao(): NewsDao

    companion object {
        const val DB_NAME = "db"
    }

}