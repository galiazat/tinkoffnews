package com.tinkoff.tinkoffnews.data.network.news

import com.tinkoff.tinkoffnews.data.network.entity.BaseResponse
import com.tinkoff.tinkoffnews.data.network.news.entity.FullNewsRO
import com.tinkoff.tinkoffnews.data.network.news.entity.NewsRO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *   @author Azat Galiullin
 **/
interface NewsApi {

    @GET("news")
    fun getNews(): Single<BaseResponse<List<NewsRO>>>

    @GET("news_content")
    fun getFullNews(@Query("id") id: Long): Single<BaseResponse<FullNewsRO>>

}