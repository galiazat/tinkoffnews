package com.tinkoff.tinkoffnews

import android.app.Application
import com.facebook.stetho.Stetho
import com.tinkoff.tinkoffnews.di.defaultAppModules
import org.koin.android.ext.android.startKoin

/**
 *   @author Azat Galiullin
 **/
class App : Application() {

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Stetho.initializeWithDefaults(this)
        startKoin(this, defaultAppModules)
    }
}