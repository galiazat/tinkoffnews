package com.tinkoff.tinkoffnews.repositories.news

import com.tinkoff.tinkoffnews.domain.news.entity.News
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
interface NewsDbRepository {
    fun getNewsList(): Single<List<News>>
    fun saveNewsList(newsList: List<News>): Single<Unit>
}