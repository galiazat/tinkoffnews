package com.tinkoff.tinkoffnews.repositories.news.impl

import com.tinkoff.tinkoffnews.data.db.news.NewsDao
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.repositories.news.NewsDbRepository
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
class NewsDbRepositoryImpl(private val newsDao: NewsDao) : NewsDbRepository {
    override fun getNewsList(): Single<List<News>> = newsDao.getNews()

    override fun saveNewsList(newsList: List<News>): Single<Unit> = Single.fromCallable {
        newsDao.clearTable()
        newsDao.insertNews(newsList)
    }
}