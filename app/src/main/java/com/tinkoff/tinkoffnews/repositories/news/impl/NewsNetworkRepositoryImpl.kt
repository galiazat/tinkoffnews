package com.tinkoff.tinkoffnews.repositories.news.impl

import com.tinkoff.tinkoffnews.data.network.entity.EmptyPayloadException
import com.tinkoff.tinkoffnews.data.network.news.NewsApi
import com.tinkoff.tinkoffnews.domain.news.entity.FullNews
import com.tinkoff.tinkoffnews.repositories.news.NewsNetworkRepository
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
class NewsNetworkRepositoryImpl(private val newsApi: NewsApi) : NewsNetworkRepository {
    override fun getNewsList() = newsApi.getNews().map {
        it.payload?.map { it.convertToNews() } ?: listOf()
    }!!

    override fun getFullNews(id: Long): Single<FullNews> = newsApi.getFullNews(id).map {
        it.payload?.convertToFullNews() ?: throw EmptyPayloadException(it.resultCode)
    }
}