package com.tinkoff.tinkoffnews.repositories.news

import com.tinkoff.tinkoffnews.domain.news.entity.FullNews
import com.tinkoff.tinkoffnews.domain.news.entity.News
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
interface NewsNetworkRepository {
    fun getNewsList(): Single<List<News>>
    fun getFullNews(id: Long): Single<FullNews>
}