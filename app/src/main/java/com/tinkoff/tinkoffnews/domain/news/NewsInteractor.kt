package com.tinkoff.tinkoffnews.domain.news

import com.tinkoff.tinkoffnews.domain.news.entity.FullNews
import com.tinkoff.tinkoffnews.domain.news.entity.News
import com.tinkoff.tinkoffnews.repositories.news.NewsDbRepository
import com.tinkoff.tinkoffnews.repositories.news.NewsNetworkRepository
import com.tinkoff.tinkoffnews.util.rxSchedulers.RxSchedulers
import io.reactivex.Single

/**
 *   @author Azat Galiullin
 **/
class NewsInteractor(private val newsDbRepository: NewsDbRepository,
                     private val newsNetworkRepository: NewsNetworkRepository,
                     private val rxSchedulers: RxSchedulers) {

    fun getFullNews(id: Long): Single<FullNews> = newsNetworkRepository.getFullNews(id)

    fun getNewsList(): Single<List<News>> =
            newsNetworkRepository.getNewsList()
                    .doOnSuccess { saveNewsListInDbParallel(it) }
                    .onErrorResumeNext {
                        it.printStackTrace()
                        newsDbRepository.getNewsList()
                    }

    private fun saveNewsListInDbParallel(newsList: List<News>) {
        newsDbRepository.saveNewsList(newsList)
                .subscribeOn(rxSchedulers.dbScheduler)
                .subscribe()
    }
}