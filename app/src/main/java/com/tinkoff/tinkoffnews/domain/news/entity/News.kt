package com.tinkoff.tinkoffnews.domain.news.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.tinkoff.tinkoffnews.util.date.DefaultFormattedDateDelegate

/**
 *   @author Azat Galiullin
 **/
@Entity
data class News(@field:PrimaryKey val id: Long,
                val name: String,
                val text: String,
                val publicationDate: Long,
                val bankInfoTypeId: Int) : Parcelable {
    @delegate:Ignore
    val defaultFormattedPublicationDate by DefaultFormattedDateDelegate(publicationDate)

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(text)
        parcel.writeLong(publicationDate)
        parcel.writeInt(bankInfoTypeId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<News> {
        override fun createFromParcel(parcel: Parcel): News {
            return News(parcel)
        }

        override fun newArray(size: Int): Array<News?> {
            return arrayOfNulls(size)
        }
    }
}