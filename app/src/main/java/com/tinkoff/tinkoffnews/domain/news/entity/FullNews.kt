package com.tinkoff.tinkoffnews.domain.news.entity

/**
 *   @author Azat Galiullin
 **/
data class FullNews(val title: NewsTitle,
                    val creationDate: Long,
                    val lastModificationDate: Long,
                    val content: String,
                    val bankInfoTypeId: Int,
                    val typeId: String) {
}