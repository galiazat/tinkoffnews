package com.tinkoff.tinkoffnews.domain.news.entity

/**
 *   @author Azat Galiullin
 **/
data class NewsTitle(val id: String,
                     val name: String,
                     val text: String,
                     val publicationDate: Long,
                     val bankInfoTypeId: Int)